#!/bin/tcsh -f
# BNMRFIT installation script

# Downloading recent copy of BNMRFIT
wget -nc http://bnmr.triumf.ca/downloads/bnmrfit.tgz

# Setting installation directory
set INSTDIR = `pwd`
setenv INSTDIR `pwd`
echo -n 'Choose installation directory ['$INSTDIR']: '
set INSTDIR = $<
if ( $INSTDIR == "") then
    set INSTDIR = `pwd`
endif 

# Extracting BNMRFIT scripts into installation directory
echo 'Installing in '$INSTDIR
tar -xzf bnmrfit.tgz -C $INSTDIR
# Clean up
rm -f bnmrfit.tgz

# Setting data directory
echo -n 'Do you have isdaq01 data directory structure? [No]/Yes: '
set ANS = $<
if ( $ANS[1] == "Yes" || $ANS[1] == "yes" ) then
    replace isdaqstruct=0 isdaqstruct=1 -- $INSTDIR/bnmrfit/bnmr*.pcm
else
echo -n 'Where is the data directory [/mnt/data]: '
set DATADIR = $<
if ( $DATADIR != "" ) then
    replace '/mnt/data' $DATADIR -- $INSTDIR/bnmrfit/bnmr*
    .pcm
endif
endif

echo 'Finished installing. Thank you for using BNMRFit'
