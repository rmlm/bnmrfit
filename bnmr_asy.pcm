!
! *** BNMR_ASY.PCM ***                
! 
! by Zaher on Dec. 2002
! Changed by Zaher on Jun. 2005
!
! starting from pcm files of B. Hitti and T. Beals
!
! This returns the frquency in vector BFRQ, the asymmetry in a matrix 
! BASY[scan number,asymmetry value] and the error in a matrix
! BAERR[scan number, asymmetry value] 
!
SET CURSOR -1
SET CHARSZ 0

! frequency scan flag (0 not frequency scan, 1 is a frequency scan)
frqflag=0

! Flags for the histograms found
fpflag=0
fmflag=0
bpflag=0
bmflag=0

! Na cell scan flag (0 not Na cell scan, 1 is Na cell scan)
cellflag=0

! TD-BNMR flag
tdflag=0

! Neutral beam counters flags
nbfpflag=0
nbfmflag=0
nbbpflag=0
nbbmflag=0

nbfp=0
nbfm=0
nbbp=0
nbbm=0
nbsum=1


! Checking the titles of histograms:
do i=[1:ncols]
  
!   Frequency histogram
  if (eqs(histtitle[i],FRQlabel[1])|eqs(histtitle[i],FRQlabel[2])) then
    frq=ih[*,i]/1000.           ! Frequency in kHz
    frqflag=1
  endif
  
!   Voltage on Na cell histogram
  if (eqs(histtitle[i],Celllabel[1])|eqs(histtitle[i],Celllabel[2])) then
    cell=ih[*,i]/1000.          !Voltage in Volts
    cellflag=1
  endif

!   Field in field sweep
  if (eqs(histtitle[i],Fieldlabel[1])|eqs(histtitle[i],Fieldlabel[2])|eqs(histtitle[i],Fieldlabel[3])) then
    field=ih[*,1]          !Field in Gauss
    fieldflag=1
  endif
  
!   BNMR Counters
!   Forward counter for positive helicity
  if ((eqs(histtitle[i],Fplabel[1])|eqs(histtitle[i],Fplabel[2])|eqs(histtitle[i],Fplabel[3])) & eqs(expt,`BNMR')) then
    fp=ih[*,i]
    fpflag=1
  endif
!   Forward counter for negative helicity
  if ((eqs(histtitle[i],Fmlabel[1])|eqs(histtitle[i],Fmlabel[2])|eqs(histtitle[i],Fmlabel[3])) & eqs(expt,`BNMR')) then
    fm=ih[*,i]
    fmflag=1
  endif
!   Backward counter for positive helicity
  if ((eqs(histtitle[i],Bplabel[1])|eqs(histtitle[i],Bplabel[2])|eqs(histtitle[i],Bplabel[3])) & eqs(expt,`BNMR')) then
    bp=ih[*,i]
    bpflag=1
  endif
!   Backward counter for negative helicity
  if ((eqs(histtitle[i],Bmlabel[1])|eqs(histtitle[i],Bmlabel[2])|eqs(histtitle[i],Bmlabel[3])) & eqs(expt,`BNMR')) then
    bm=ih[*,i]
    bmflag=1
  endif
  
!   BNQR Counters
!   Right counter for positive helicity
  if ((eqs(histtitle[i],PolRplabel[1])|eqs(histtitle[i],PolRplabel[2])) & eqs(expt,`BNQR')) then
    fp=ih[*,i]
    fpflag=1
  endif
!   Right counter for negative helicity
  if ((eqs(histtitle[i],PolRmlabel[1])|eqs(histtitle[i],PolRmlabel[2])) & eqs(expt,`BNQR')) then
    fm=ih[*,i]
    fmflag=1
  endif
!   Left counter for positive helicity
  if ((eqs(histtitle[i],PolLplabel[1])|eqs(histtitle[i],PolLplabel[2])) & eqs(expt,`BNQR')) then
    bp=ih[*,i]
    bpflag=1
  endif
!   Left counter for negative helicity
  if ((eqs(histtitle[i],PolLmlabel[1])|eqs(histtitle[i],PolLmlabel[2])) & eqs(expt,`BNQR')) then
    bm=ih[*,i]
    bmflag=1
  endif
  
!   Nueatral beam counters
!   Forward counter for positive helicity
  if (eqs(histtitle[i],NBFplabel[1])|eqs(histtitle[i],NBFplabel[2])) then
    nbfp=ih[*,i]
    nbfpflag=1
  endif
!   Forward counter for negative helicity
  if (eqs(histtitle[i],NBFmlabel[1])|eqs(histtitle[i],NBFmlabel[2])) then
    nbfm=ih[*,i]
    nbfmflag=1
  endif
!   Backward counter for positive helicity
  if (eqs(histtitle[i],NBBplabel[1])|eqs(histtitle[i],NBBplabel[2])) then
    nbbp=ih[*,i]
    nbbpflag=1
  endif
!   Backward counter for negative helicity
  if (eqs(histtitle[i],NBBmlabel[1])|eqs(histtitle[i],NBBmlabel[2])) then
    nbbm=ih[*,i]
    nbbmflag=1
  endif
enddo                           ! Finished with the important histograms, the rest are
                                ! ignored for now

! Deal with Fast mode
if (eqs(method,methodd)) then
  bp=ih[*,5]-ih[*,1]
  bpflag=1
  fp=ih[*,6]-ih[*,2]
  fpflag=1
  bm=ih[*,7]-ih[*,3]
  bmflag=1
  fm=ih[*,8]-ih[*,4]
  fmflag=1
endif



! Due to some problems in the histogram naming.
if (eqs(method,methodc)&(bpflag=0 | bmflag=0 | fpflag=0 | fmflag=0)) then   !for now the histogram labels are wrong 
  display `WARNING: Stupid problem with histograms names'
!   BNQR TD Counters
!   Left counter for positive helicity
  method=methodc
  bp=ih[*,3]
  bpflag=1
!   Right counter for positive helicity
  fp=ih[*,4]
  fpflag=1
!   Left counter for negative helicity
  bm=ih[*,5]
  bmflag=1
!   Right counter for negative helicity
  fm=ih[*,6]
  fmflag=1
endif  

if (cellflag=1) then 
  display `Na Cell scan detected'
  frq=cell
endif

if (fieldflag=1) then 
  display `Field scan detected'
  frq=field
endif

if (frqflag=1) then 
  display `Frequency scan detected'
  if (frqflag=1 & bpflag=1 & bmflag=1 & fpflag=1 & fmflag=1) then
    display `All BNMR counters found!'
  endif
endif

! AsyFB for runs that can be dealt with
if (bpflag=1 & bmflag=1 & fpflag=1 & fmflag=1) then
!   Deadtime correction from J.
  if (dtflag=1) then
!     Dead time must be corrected for each counter
!     the value for the dead time is the last term in each expression
!     i.e .0000000450020547. This was the value I calculated from the beam off 
!     experiments 
! RM changes: take out next 4 lines
!    fm = (fm/(.01*60))/(1-((fm/(.01*60))*deadtime))
!    bm = (bm/(.01*60))/(1-((bm/(.01*60))*deadtime))
!    bp = (bp/(.01*60))/(1-((bp/(.01*60))*deadtime))
!    fp = (fp/(.01*60))/(1-((fp/(.01*60))*deadtime))
    fm = (fm)/(1-((fm)*deadtime))
    bm = (bm)/(1-((bm)*deadtime))
    bp = (bp)/(1-((bp)*deadtime))
    fp = (fp)/(1-((fp)*deadtime))
  endif
  @dir_2//`bnmr_yab'
endif  

! Now deal with finding F and B counts
if (bpflag=0 | bmflag=0 | fpflag=0 | fmflag=0 | fnorm=5) then
  display `Calculating asymmetry from user defined counters'
!   Ask how many asymmeties to generate
  asyinq:
  inquire `How many asymmetries do you need ['//rchar(nasy)//`]? >>' nasy
  
  aborted=0
  if (nasy>4) then
    display `Get real'
    goto asyinq
  endif
  if (nasy<1) then
    aborted=1
    goto abort
  endif

!   For now accept only 1,2 or 4
  if (nasy~=1 | nasy~=2 | nasy~=4) then
    display `Can handle only 1,2 or 4 asymmetries. You want more teach me.'
  endif

  if (exist(`AsyFBm')) then destroy AsyFBm
  do iasy=[1:nasy]
!     if you did not find all histograms needed to generate asymmetry
!     ask for help from user 
    @dir_2//`bnmr_asygrl'
    if (aborted=1) then goto abort
!     Calculate the asymmetry for all scans
    @dir_2//`bnmr_yab'
    if (iasy=1) then 
      AsyFBm=AsyFB
      Asyerrm=Asyerr
    endif
    if (iasy~=1) then
      AsyFBm=[AsyFBm;AsyFB]
      Asyerrm=[Asyerrm;Asyerr]
    endif
  enddo

!   On the AsyFB level we can subtract different bins, much easier this way
  
!   If nasy=1, normal asymmetry do nothing you still have AsyFB and Asyerr
!   If nasy=2 then subtract one from the other
  if (nasy=2) then
    AsyFB=AsyFBm[1:npts,1]-AsyFBm[1:npts,2]
    Asyerr=sqrt(Asyerrm[1:npts,1]*Asyerrm[1:npts,1]+Asyerrm[1:npts,2]*Asyerrm[1:npts,2])
  endif
!   If nasy=4 then need to calculate slopes and signal
  if (nasy=4) then
!     There are two options as far as I know, Asy1+Asy2-Asy3-Asy4
!     or fit to linear Asy1&Asy2 and Asy3&Asy4.
    display `How to combine the 4 asymmetries:'
    display `1- Asy1+Asy2-Asy3-Asy4.
    display `2- Fit slopes'
    fourasyinq:
    txt=robmode
    inquire `Chose combination scheme ['//rchar(txt)//`] >>' txt
    if (txt<1 | txt>2) then goto fourasyinq
    robmode=txt
    if (txt=1) then
      AsyFB=AsyFBm[1:npts,1]+AsyFBm[1:npts,2]-AsyFBm[1:npts,3]-AsyFBm[1:npts,4]
      Asyerr=Asyerrm[1:npts,1]*Asyerrm[1:npts,1]+Asyerrm[1:npts,2]*Asyerrm[1:npts,2]
      Asyerr=Asyerr+Asyerrm[1:npts,3]*Asyerrm[1:npts,3]+Asyerrm[1:npts,4]*Asyerrm[1:npts,4]
      Asyerr=sqrt(Asyerr)
    endif
    if (txt=2) then
      display `God this is too hard, still working on it..'
    endif
  endif
endif

! and now for separate scans
@dir_2//`bnmr_asycalc'

! Abort something failed
abort:
